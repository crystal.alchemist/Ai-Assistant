# Change Log

## 0.0.7 Ollama ready
- **New Features :** Ollama now supported. Check <a href="https://ollama.com/">ollama.com</a> for installation instructions and documentation.
- **New Features :** Quick model switch feature. Use `/setmodel <model_name>` to quickly switch models, `/setmodel rm` to remove the custom model and revert to a default selection.
- **Bug Fix:** Git Commit AI generator issue resolved.
- Various small improvements and bug fixes.

## 0.0.6
- **Improvement:** Style changes compatibility with all themes.
- **Improvement:** Compatibility with VSCode 1.86 and above.
- **Bug Fix:** Issue with missing Custom Prompt text field.

## 0.0.5
- Miscellaneous bug fixes.

## 0.0.4
- **Improvement:** New Conversation History page.
- **Bug Fix:** Styling issues for buttons, menu, and chat response editor.
- Additional bug fixes.


## 0.0.3
- **Bug Fix:** Styling issues for buttons and chat response editor.
- Other minor bugs.

## 0.0.2
- **Bug Fix:** Intermittent malfunction of Clear API function.

## 0.0.1
- Initial release.